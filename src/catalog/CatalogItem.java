package catalog;

import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class CatalogItem {
  protected Classification classification;
  protected String vendor;
  protected String model;
  protected LocalDate dateOfManufacture;
  protected String color;
  protected boolean outOfStock;
  protected BigInteger count;
  protected BigDecimal price;

  private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

  CatalogItem(catalog.protocol.CatalogItemType xmlElement) {
    classification = new Classification(xmlElement.getClassification());
    vendor = xmlElement.getVendor();
    model = xmlElement.getModel();
    dateOfManufacture = LocalDate.parse(xmlElement.getDateOfManufacture(), dateFormatter);
    color = xmlElement.getColor();
    outOfStock = xmlElement.getOutOfStock() != null;
    if (!outOfStock) {
      count = xmlElement.getCount();
      price = xmlElement.getPrice();
    }
  }

  public void prettyPrint(PrintStream out, String indent) {
    String indent2 = indent + "   ";
    out.printf("%sТоварная позиция%n", indent);
    classification.prettyPrint(out, indent2);
    out.printf("%sПроизводитель: %s%n", indent2, vendor);
    out.printf("%sМодель: %s%n", indent2, model);
    out.printf("%sДата изготовления: %s%n", indent2, dateOfManufacture.toString());
    out.printf("%sЦвет: %s%n", indent2, color);
    if (outOfStock) {
      out.printf("%sНет в наличии%n", indent2);
    } else {
      out.printf("%sВ наличии: %s%n", indent2, count.toString());
      out.printf("%sЦена: %s%n", indent2, price.toString());
    }
  }
}
