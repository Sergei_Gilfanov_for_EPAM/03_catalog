package catalog;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class Catalog {
  List<CatalogItem> catalogItems;

  Catalog(catalog.protocol.Catalog xmlElement) {
    catalogItems = new ArrayList<CatalogItem>();
    for (catalog.protocol.CatalogItemType item : xmlElement.getCatalogItem()) {
      catalogItems.add(new CatalogItem(item));
    }
  }

  public void prettyPrint(PrintStream out, String indent) {
    String indent2 = indent + "   ";
    for (CatalogItem item : catalogItems) {
      item.prettyPrint(out, indent2);
    }
  }
}
