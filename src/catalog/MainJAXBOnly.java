package catalog;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventHandler;
import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Schema;

import org.xml.sax.SAXException;

public class MainJAXBOnly {

  public static void main(String[] args) {
    FileInputStream input = null;
    try {
      SchemaFactory sf = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
      Schema schema = sf.newSchema(new File("xml/catalog.xsd"));

      JAXBContext context = JAXBContext.newInstance("catalog.protocol");
      Unmarshaller unmarshaller = context.createUnmarshaller();
      unmarshaller.setSchema(schema);
      unmarshaller.setEventHandler(new ValidationEventHandler() {
        public boolean handleEvent(ValidationEvent ve) {
          System.out.println(ve.getLinkedException());
          return true;
        }
      });

      input = new FileInputStream("xml/catalog.xml");
      catalog.protocol.Catalog parsed = (catalog.protocol.Catalog) unmarshaller.unmarshal(input);
      Catalog catalog = new Catalog(parsed);

      catalog.prettyPrint(System.out, "");

    } catch (JAXBException | SAXException | IOException ex) {
      ex.printStackTrace();
    }
  }

}
