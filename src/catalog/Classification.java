package catalog;

import java.io.PrintStream;

public class Classification {
  String category;
  String subcategory;

  Classification(catalog.protocol.ClassificationType xmlElement) {
    category = xmlElement.getCategory();
    subcategory = xmlElement.getSubcategory();
  }

  public void prettyPrint(PrintStream out, String indent) {
    out.printf("%sКлассификация: %s/%s%n", indent, category, subcategory);
  }

}
