package catalog;

import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.XMLEvent;

import catalog.protocol.*;

public class MainStAXandJAXB {

  /**
   * Сдвигаем позицию чтения в xmlReader до события XMLEvent.START_ELEMENT, но оставляем его
   * непрочитанным
   * 
   * @param xmlReader
   * @return найденное событие (оно же будет прочитано при вызове xmlReader.next) или null, если
   *         больше таких событий не найдено
   * @throws XMLStreamException
   */
  private static XMLEvent skipToStartEvent(XMLEventReader xmlReader) throws XMLStreamException {
    XMLEvent peekEvent;
    while (true) {
      peekEvent = xmlReader.peek();
      if (peekEvent == null) {
        break;
      }
      if (peekEvent.getEventType() == XMLEvent.START_ELEMENT) {
        break;
      }
      xmlReader.nextEvent();
    }
    return peekEvent;
  }

  /**
   * Сдвигаем позицию чтения xmlReader до cобытия открытия тега catalogItem, но оставляем его
   * непрочитанным
   * 
   * @param xmlReader
   * @return найденное событие (оно же будет прочитано при вызове xmlReader.next) или null, если
   *         больше таких событий не найдено
   * @throws XMLStreamException
   */
  private static XMLEvent skipToCatalogItem(XMLEventReader xmlReader) throws XMLStreamException {
    // namespace для в всех сгенерированных классов находится в аннотации XmlSchema пакета всех
    // этих классов.
    final String elementNamespace =
        CatalogItemType.class.getPackage().getAnnotation(XmlSchema.class).namespace();
    final QName catalogItemName = new QName(elementNamespace, "catalogItem");

    XMLEvent peekEvent;
    while (true) {
      peekEvent = skipToStartEvent(xmlReader);
      if (peekEvent == null) {
        break;
      }
      QName name = peekEvent.asStartElement().getName();
      if (name.equals(catalogItemName)) {
        break;
      }
      xmlReader.nextEvent();
    }
    return peekEvent;
  }

  public static void main(String[] args) {
    FileInputStream input = null;
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(CatalogItemType.class);
      Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

      input = new FileInputStream("xml/catalog.xml");

      XMLInputFactory xmlif = XMLInputFactory.newInstance();
      xmlif.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, Boolean.TRUE);
      xmlif.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, Boolean.FALSE);
      xmlif.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, Boolean.TRUE);
      xmlif.setProperty(XMLInputFactory.IS_COALESCING, Boolean.TRUE);
      XMLEventReader xmlReader = xmlif.createXMLEventReader(input);

      while (skipToCatalogItem(xmlReader) != null) {
        JAXBElement<CatalogItemType> element =
            unmarshaller.unmarshal(xmlReader, CatalogItemType.class);
        CatalogItem catalogItem = new CatalogItem(element.getValue());
        catalogItem.prettyPrint(System.out, "   ");
      }
    } catch (IOException | XMLStreamException | JAXBException ex) {
      ex.printStackTrace();
      return;
    }
  }
}
